package com.epam.training.exercise1;

import java.io.IOException;


/**
 * Main class.
 */
public final class AdvancedAscii {
    private AdvancedAscii() {
    }

    /**
     * Main method.
     * @param args console args
     */
    public static void main(String[] args) {
        try {
            Image image = Image.createImage("pair_hiking.png");
            ConsolePrintService.printImage(image);
        } catch (IOException e) {
            System.out.println("Image cannot be printed.");
        }
    }
}
