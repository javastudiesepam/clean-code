package com.epam.training.exercise1;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.Objects;

import javax.imageio.ImageIO;

/**
 * A class that represents an image.
 * Allows to read an image from a file and perform next operations on it:
 * get image height
 * get image width
 * get rgb values of a dot on the image by coordinates
 */
public final class Image {

    /**
     * last byte size.
     */
    private static final int LAST_BYTE = 0xFF;
    /**
     * byte size.
     */
    private static final int BYTE = 8;
    /**
     * two bytes size.
     */
    private static final int TWO_BYTES = 16;

    /**
     * image read from a file.
     */
    private final transient BufferedImage bufferedImage;

    private Image(final String fileName) throws NoFileException {
        this.bufferedImage = loadImageFromFile(fileName);
    }

    /**
     * Reads an image from a file and returns it.
     * @param fileName a name of a file to be read
     * @return Image instance created from a file
     * @throws NoFileException when file does not exist
     */
    public static Image createImage(final String fileName) throws NoFileException {
        return new Image(fileName);
    }

    /**
     * Calculates and returns image height.
     * @return image height
     */
    public int getHeight() {
        return bufferedImage.getHeight();
    }

    /**
     * Calculates and returns image width.
     * @return image width
     */
    public int getWidth() {
        return bufferedImage.getWidth();
    }

    /**
     * Calculates and returns red value of a dot defined by coordinates.
     * @param coordinate dot coordinate
     * @return red value
     */
    public int getRed(final Coordinate coordinate) {
        final int rgbValue = getRgbValue(coordinate);
        return (rgbValue >> TWO_BYTES) & LAST_BYTE;
    }

    /**
     * Calculates and returns green value of a dot defined by coordinates.
     * @param coordinate dot coordinate
     * @return green value
     */
    public int getGreen(final Coordinate coordinate) {
        final int rgbValue = getRgbValue(coordinate);
        return (rgbValue >> BYTE) & LAST_BYTE;
    }

    /**
     * Calculates and returns blue value of a dot defined by coordinates.
     * @param coordinate dot coordinate
     * @return blue value
     */
    public int getBlue(final Coordinate coordinate) {
        final int rgbValue = getRgbValue(coordinate);
        return rgbValue & LAST_BYTE;
    }

    private int getRgbValue(final Coordinate coordinate) {
        if (coordinate.getX() < 0 || coordinate.getX() > bufferedImage.getWidth()) {
            throw new IllegalArgumentException("Coordinate x out of range: 0.." + bufferedImage.getWidth());
        } else if (coordinate.getY() < 0 || coordinate.getY() > bufferedImage.getHeight()) {
            throw new IllegalArgumentException("Coordinate y out of range: 0.." + bufferedImage.getHeight());
        }
        return bufferedImage.getRGB(coordinate.getX(), coordinate.getY());
    }

    private BufferedImage loadImageFromFile(final String fileName) throws NoFileException {
        try {
            final URL url = Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResource(fileName));
            return ImageIO.read(url);
        } catch (IOException exception) {
            throw new NoFileException("File not found!", exception);
        }
    }
}
