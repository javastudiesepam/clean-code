package com.epam.training.exercise1;

import java.io.IOException;

/**
 * Class represents an exception that occurs when there is no file.
 */
/* package */ class NoFileException extends IOException {
    public static final long serialVersionUID = 4328743;
    /* package */ NoFileException(final String errorMessage, final Throwable err) {
        super(errorMessage, err);
    }
}
