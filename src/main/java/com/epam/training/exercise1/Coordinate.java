package com.epam.training.exercise1;

/**
 * Class represents coordinate.
 */
public class Coordinate {

    /**
     * x coordinate
     */
    private transient final int abscissa;

    /**
     * y coordinate
     */
    private transient final int ordinate;

    /**
     * @param abscissa
     * @param ordinate
     */
    public Coordinate(final int abscissa, final int ordinate) {
        this.abscissa = abscissa;
        this.ordinate = ordinate;
    }

    /**
     * Returns x coordinate.
     * @return abscissa
     */
    public int getX() {
        return abscissa;
    }

    /**
     * Returns y coordinate.
     * @return ordinate
     */
    public int getY() {
        return ordinate;
    }

}
