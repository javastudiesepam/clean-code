package com.epam.training.exercise1;


/**
 * Class that prints image to console.
 */
final class ConsolePrintService {
    private static final char[] CHARS_BY_DARKNESS = {'#', '@', 'X', 'L', 'I', ':', '.', ' '};
    private static final int MAX_COLOR_VAL = 255;
    private static final  int COLORS_AMOUNT = 255;
    private static final int HEIGHT_DECREASE_RATIO = 60;
    private static final int WIDTH_DECREASE_RATIO = 200;

    private static int min = MAX_COLOR_VAL * COLORS_AMOUNT;
    private static int max;

    private ConsolePrintService() {
    }

    /* package */ static void printImage(Image image) {
        int stepY = image.getHeight() / HEIGHT_DECREASE_RATIO;
        int stepX = image.getWidth() / WIDTH_DECREASE_RATIO;

        calculateMinMax(image, stepY, stepX);
        printToConsole(image, stepY, stepX);
    }

    private static void calculateMinMax(Image image, int stepY, int stepX) {
        for (int y = 0; y < image.getHeight(); y += stepY) {
            for (int x = 0; x < image.getWidth(); x += stepX) {
                int sum = 0;
                for (int avgy = 0; avgy < stepY; avgy++) {
                    for (int avgx = 0; avgx < stepX; avgx++) {
                        sum = sum + (image.getRed(new Coordinate(x, y)) + image.getBlue(new Coordinate(x, y)) + image.getGreen(new Coordinate(x, y)));
                    }
                }

                sum = sum / stepY / stepX;
                if (max < sum) {
                    max = sum;
                }
                if (min > sum) {
                    min = sum;
                }
            }
        }
    }

    private static void printToConsole(Image image, int stepY, int stepX) {
        for (int y = 0; y < image.getHeight() - stepY; y += stepY) {
            for (int x = 0; x < image.getWidth() - stepX; x += stepX) {
                int sum = 0;
                for (int avgy = 0; avgy < stepY; avgy++) {
                    for (int avgx = 0; avgx < stepX; avgx++) {
                        sum = sum + (image.getRed(new Coordinate(x, y)) + image.getBlue(new Coordinate(x, y)) + image.getGreen(new Coordinate(x, y)));
                    }
                }
                sum = sum / stepY / stepX;
                System.out.print(CHARS_BY_DARKNESS[(sum - min) * CHARS_BY_DARKNESS.length / (max - min + 1)]);
            }
            System.out.println();
        }
    }
}
