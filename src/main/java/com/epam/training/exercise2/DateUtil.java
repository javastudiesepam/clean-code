package com.epam.training.exercise2;

import java.util.Calendar;
import java.util.Date;

/**
 *  Class represents a set of tools to work with time.
 */
public final class DateUtil {
    private DateUtil() {
    }

    /**
     * Method increments or decrements date by 1.
     * @param date - date that should be incremented
     * @param increment - if true - incremented else decremented
     */
    public static void changeDateByOneDay(final Date date, final boolean increment) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, increment ? 1 : -1);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        date.setTime(calendar.getTime().getTime());
    }

    /**
     * Methods creates date objects.
     * @param year - year that should be set
     * @param month - month that should be set
     * @param day - day that should be set
     * @return date object
     */
    public static Date dateFactory(final int year, final int month, final int day) {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        return calendar.getTime();
    }

    /**
     * Main method.
     * @param args - arguments.
     */
    public static void main(final String[] args) {
        final Date date = new Date();
        changeDateByOneDay(date, false);
        System.out.println(date);

        System.out.println(DateUtil.dateFactory(2014, 10, 10));
    }

}
