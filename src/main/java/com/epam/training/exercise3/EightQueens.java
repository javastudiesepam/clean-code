package com.epam.training.exercise3;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * Class represents the eight queens puzzle solutions.
 */
public final class EightQueens {
    /**
     * Square board size.
     */
    private static final int BOARD_SIZE = 8;
    private static final char QUEENS_SYMBOL = 'q';
    private static final char EMPTY_SYMBOL = '.';

    private EightQueens() {
    }


    /**
     * Main method. Outputs solutions for eight queens puzzle to the console.
     *
     * @param args - a list of arguments.
     */
    public static void main(String[] args) {
        ArrayList<char[][]> solutions = new ArrayList<>();
        char[][] board = new char[BOARD_SIZE][BOARD_SIZE];

        for (int i = 0; i < BOARD_SIZE; i++) {
            Arrays.fill(board[i], EMPTY_SYMBOL);
        }

        solveAllNQueens(board, 0, solutions);
        printSolutionsToConsole(solutions);
    }

    private static void solveAllNQueens(char[][] board, int col, ArrayList<char[][]> solutions) {
        if (col == board.length) {
            addBoard(board, solutions);
        } else {
            for (int row = 0; row < board.length; row++) {
                board[row][col] = 'q';

                if (queensAreSafe(board)) {
                    solveAllNQueens(board, col + 1, solutions);
                }

                board[row][col] = '.';
            }
        }
    }

    private static void addBoard(char[][] board, ArrayList<char[][]> solutions) {
        char[][] copy = new char[board.length][board[0].length];

        for (int i = 0; i < board.length; i++) {
            System.arraycopy(board[i], 0, copy[i], 0, board[0].length);
        }
        solutions.add(copy);
    }

    /**
     * determine if the chess board represented
     * by board is a safe set up
     * board is a square matrix.
     * all elements of board == 'q' or '.'. 'q's represent
     * queens, '.' represent open spaces.
     * @param board the chessboard
     * @return true if the configuration of board is safe
     */
    private static boolean queensAreSafe(char[][] board) {
        return hasMoreThanOneQueenInCol(board)
            && hasMoreThanOneQueenInRow(board)
            && hasMoreThanOneQueenInDiagonal(board)
            && hasMoreThanOneQueenInBackDiagonal(board);
    }

    private static boolean hasMoreThanOneQueenInDiagonal(char[][] board) {
        boolean isSafe = true;
        for (int offset = -board.length; offset < board.length; offset++) {
            boolean found = false;
            for (int i = 0; i < board.length; i++) {
                if (inbounds(i, board.length - offset - i - 1, board)) {
                    char cell = board[i][board.length - offset - i - 1];
                    isSafe = isSafe(cell, found, isSafe)[0];
                    found = isSafe(cell, found, isSafe)[1];
                }
            }
        }
        return isSafe;
    }

    private static boolean hasMoreThanOneQueenInBackDiagonal(char[][] board) {
        boolean isSafe = true;
        for (int offset = -board.length; offset < board.length; offset++) {
            boolean found = false;
            for (int i = 0; i < board.length; i++) {
                if (inbounds(i, i + offset, board)) {
                    char cell = board[i][i + offset];
                    isSafe = isSafe(cell, found, isSafe)[0];
                    found = isSafe(cell, found, isSafe)[1];
                }
            }
        }
        return isSafe;
    }

    private static boolean hasMoreThanOneQueenInRow(char[][] board) {
        boolean isSafe = true;
        for (char[] aBoard : board) {
            boolean found = false;
            for (char cell : aBoard) {
                isSafe = isSafe(cell, found, isSafe)[0];
                found = isSafe(cell, found, isSafe)[1];
            }
        }
        return isSafe;
    }

    private static boolean hasMoreThanOneQueenInCol(char[][] board) {
        boolean isSafe = true;
        for (int i = 0; i < board.length; i++) {
            boolean found = false;
            for (char[] aBoard : board) {
                isSafe = isSafe(aBoard[i], found, isSafe)[0];
                found = isSafe(aBoard[i], found, isSafe)[1];
            }
        }
        return isSafe;
    }

    /**
     * Checks if queen can be safe.
     * @param element - board cell
     * @param found - indicates if a queen was already found in this sequence
     * @return array of boolean values - first if queen is safe, second if queen was found in this element(cell)
     */
    private static boolean[] isSafe(char element, boolean found, boolean isSafe) {
        boolean[] result = new boolean[] {isSafe, found};

        if (element == QUEENS_SYMBOL) {
            if (found) {
                result[0] = false;
            }
            result[1] = true;
        }
        return result;
    }

    private static boolean inbounds(int row, int col, char[][] bord) {
        return row >= 0 && row < bord.length && col >= 0 && col < bord[0].length;
    }

    private static void printSolutionsToConsole(ArrayList<char[][]> solutions) {
        System.out.println(solutions.size());
        for (int i = 0; i < solutions.size(); i++) {
            System.out.println("\nSolution " + (i + 1));
            char[][] board = solutions.get(i);

            for (char[] aBoard : board) {
                for (char anABoard : aBoard) {
                    System.out.print(anABoard);
                }
                System.out.println();
            }
        }
    }
}
